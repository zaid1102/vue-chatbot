
Chat Application Using Vue.js and Firebase.

Application was made using various tutorials regarding this particular topic.

Requirements:
Basic and most essential requirement is npm.
Firebase Account
And a hosting partner.

npm helped in rapid development of application, whereas firebase acts as a database which is connected to application.
src/ contains all the required directories and files.
1. src/main.js, essentials are imported in this file which can be used in application else where.
2. src/router.js, all routing paths are defined in this file.
3. src/view, this directory stores all the view files from which are visible to user on screen.
4. src/assets, images and other required addons can be placed here.
5. src/components, it contains custom components which can be used in view.
6. src/firebase/init.js, Firebase is configured in a sepetate directory and then imported whnever it i requred.

Application is source controlled using GIT.

directory can be accessed on git using below path, https://zaid1102@bitbucket.org/zaid1102/vue-chatbot.git